﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;

using Server;
using System.Collections.Generic;

namespace UnitTest
{
    [TestClass]
    public class UnitTest1
    {
        [TestMethod]
        public void TestMethod1()
        {
            Game g = new Game();

            List<Weapon> weapons = new List<Weapon>();
            weapons.Add(new Weapon(1, "Stock", 1, 1));

            Assert.AreEqual(g.showWeaponList(weapons), "Wähle eine Waffe:\r\n1: Stock\r\n");

            Assert.AreEqual(g.useWeapon(weapons, 1), 1);

            Player player1 = new Player("Player1", 100, 55);
            Player player2 = new Player("Player2", 100, 0);

            Assert.AreEqual(g.checkIfWon(player1, player2), true);
            Assert.AreEqual(g.whoWon(player1, player2), "Spieler 1 hat gewonnen");
            player2.currentHP = 1;
            player1.currentHP = 0;
            Assert.AreEqual(g.whoWon(player1, player2), "Spieler 2 hat gewonnen");

        }
    }
}
