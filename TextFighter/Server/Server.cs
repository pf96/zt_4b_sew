﻿#define LOG
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Sockets;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Server
{
    class Server
    {
        static TcpListener listener;
        const int LIMIT = 5; //5 concurrent clients

        public static void Main()
        {
            listener = new TcpListener(2055);
            listener.Start();
#if LOG
            Console.WriteLine("Server mounted, listening to port 2055");
#endif
            for (int i = 0; i < LIMIT; i++)
            {
                Thread t = new Thread(new ThreadStart(Service));
                t.Start();
            }
        }
        public static void Service()
        {
            while (true)
            {
                Socket soc = listener.AcceptSocket();
#if LOG
                Console.WriteLine("Connected: {0}", 
                                         soc.RemoteEndPoint);

                Game g = new Game();

                List<Weapon> weapons = new List<Weapon>();
                weapons.Add(new Weapon(1, "Stock", 1, 5));
                weapons.Add(new Weapon(2, "Messer", 10, 25));

                Player player1 = new Player("Player1", 100, 100);
                Player player2 = new Player("Player2", 100, 100);

                string weaponString = g.showWeaponList(weapons);

#endif
                try
                {
                    Stream s = new NetworkStream(soc);
                    StreamReader sr = new StreamReader(s);
                    StreamWriter sw = new StreamWriter(s);
                    sw.AutoFlush = true; // enable automatic flushing

                    int request = 0;
                    string answer = "";
                    int dmg = 0;

                    while(!g.checkIfWon(player1, player2))
                    {
                        sw.WriteLine(weaponString);
                        request = sr.Read();
                        Console.WriteLine("Erhalten " + request);
                        request = 1;
                        dmg = g.useWeapon(weapons, request);
                        player2.currentHP = player2.currentHP - dmg;
                        sw.WriteLine("$Spieler 2 wurde mit "+dmg+" getroffen. Er hat noch "+player2.currentHP+" Trefferpunkte.$");

                        //sw.WriteLine("Spieler 2 ist an der Reihe!\r\n" + weaponString);
                        //request = sr.Read();
                        //dmg = g.useWeapon(weapons, request);
                        //player1.currentHP = player1.currentHP - dmg;
                        //sw.WriteLine("\r\nSpieler 1 wurde mit " + dmg + " getroffen. Er hat noch " + player2.currentHP + " Trefferpunkte.\r\n");                        
                    }
                    
                    if(g.checkIfWon(player1, player2))
                    {
                        answer = g.whoWon(player1, player2);
                        sw.WriteLine("GAME OVER$" + answer);
                    }
                       

                    s.Close();
                }
                catch (Exception e)
                {
#if LOG
                    Console.WriteLine(e.Message);
#endif
                }
#if LOG
                Console.WriteLine("Disconnected: {0}", 
                                        soc.RemoteEndPoint);
#endif
                soc.Close();
            }
        }

    }
}
