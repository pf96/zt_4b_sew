﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Server
{
    public class Weapon
    {
        public int id { get; set; }
        public string name { get; set; }
        public int minDmg { get; set; }
        public int maxDmg { get; set; }

        public Weapon(int id, string name, int minDmg, int maxDmg)
        {
            this.id = id;
            this.name = name;
            this.minDmg = minDmg;
            this.maxDmg = maxDmg;
        }
    }
}
