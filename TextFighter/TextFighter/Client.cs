﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Sockets;
using System.Text;
using System.Threading.Tasks;

namespace TextFighter
{
    class Client
    {
        public static void Main(string[] args)
        {
            TcpClient client = new TcpClient("localhost", 2055);
            try
            {
                Stream s = client.GetStream();
                s.ReadTimeout = 100;
                StreamReader sr = new StreamReader(s);

                StreamWriter sw = new StreamWriter(s);
                sw.AutoFlush = true;

                /*Console.WriteLine("schicke Hallo");
                sw.WriteLine("Hallo");
                Console.WriteLine("gesendet!");*/

                string message = "";
                char eingabe = '1';

                /*sw.WriteLine(eingabe);
                sw.Flush();
                sw.Flush();
                message = sr.ReadLine();
                Console.WriteLine("\nEmpfangen: " + message);*/

                do
                {
                    Console.WriteLine("Spieler 1 ist an der Reihe.");
                    message = sr.ReadLine(); 
                    Console.WriteLine(message);
                    eingabe = Console.ReadKey().KeyChar;
                    sw.WriteLine(eingabe);
                    message = sr.ReadLine();
                    Console.WriteLine(message);

                    
                } while (!message.StartsWith("GAME OVER"));

                Console.ReadKey();
                s.Close();
            }
            finally
            {
                // code in finally block is guranteed 
                // to execute irrespective of 
                // whether any exception occurs or does 
                // not occur in the try block
                client.Close();
            }
        }
    }
}
