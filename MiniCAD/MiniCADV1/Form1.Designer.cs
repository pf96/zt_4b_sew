﻿namespace MiniCADV1
{
    partial class Form1
    {
        /// <summary>
        /// Erforderliche Designervariable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Verwendete Ressourcen bereinigen.
        /// </summary>
        /// <param name="disposing">True, wenn verwaltete Ressourcen gelöscht werden sollen; andernfalls False.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Vom Windows Form-Designer generierter Code

        /// <summary>
        /// Erforderliche Methode für die Designerunterstützung.
        /// Der Inhalt der Methode darf nicht mit dem Code-Editor geändert werden.
        /// </summary>
        private void InitializeComponent()
        {
            this.comboBox1 = new System.Windows.Forms.ComboBox();
            this.rect_x = new System.Windows.Forms.TextBox();
            this.rect_y = new System.Windows.Forms.TextBox();
            this.rect_w = new System.Windows.Forms.TextBox();
            this.rect_h = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.button_draw = new System.Windows.Forms.Button();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.line_py = new System.Windows.Forms.TextBox();
            this.line_px = new System.Windows.Forms.TextBox();
            this.line_y = new System.Windows.Forms.TextBox();
            this.line_x = new System.Windows.Forms.TextBox();
            this.label11 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.label15 = new System.Windows.Forms.Label();
            this.circle_h = new System.Windows.Forms.TextBox();
            this.circle_w = new System.Windows.Forms.TextBox();
            this.circle_y = new System.Windows.Forms.TextBox();
            this.circle_x = new System.Windows.Forms.TextBox();
            this.SuspendLayout();
            // 
            // comboBox1
            // 
            this.comboBox1.FormattingEnabled = true;
            this.comboBox1.Items.AddRange(new object[] {
            "Rectangle",
            "Line",
            "Circle"});
            this.comboBox1.Location = new System.Drawing.Point(13, 13);
            this.comboBox1.Name = "comboBox1";
            this.comboBox1.Size = new System.Drawing.Size(121, 21);
            this.comboBox1.TabIndex = 0;
            // 
            // rect_x
            // 
            this.rect_x.Location = new System.Drawing.Point(25, 101);
            this.rect_x.Name = "rect_x";
            this.rect_x.Size = new System.Drawing.Size(40, 20);
            this.rect_x.TabIndex = 1;
            // 
            // rect_y
            // 
            this.rect_y.Location = new System.Drawing.Point(93, 101);
            this.rect_y.Name = "rect_y";
            this.rect_y.Size = new System.Drawing.Size(40, 20);
            this.rect_y.TabIndex = 2;
            // 
            // rect_w
            // 
            this.rect_w.Location = new System.Drawing.Point(25, 127);
            this.rect_w.Name = "rect_w";
            this.rect_w.Size = new System.Drawing.Size(40, 20);
            this.rect_w.TabIndex = 3;
            // 
            // rect_h
            // 
            this.rect_h.Location = new System.Drawing.Point(93, 127);
            this.rect_h.Name = "rect_h";
            this.rect_h.Size = new System.Drawing.Size(40, 20);
            this.rect_h.TabIndex = 4;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(9, 104);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(15, 13);
            this.label1.TabIndex = 5;
            this.label1.Text = "x:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(72, 104);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(15, 13);
            this.label2.TabIndex = 6;
            this.label2.Text = "y:";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(72, 130);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(16, 13);
            this.label3.TabIndex = 6;
            this.label3.Text = "h:";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(6, 130);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(18, 13);
            this.label4.TabIndex = 7;
            this.label4.Text = "w:";
            // 
            // button_draw
            // 
            this.button_draw.Location = new System.Drawing.Point(10, 619);
            this.button_draw.Name = "button_draw";
            this.button_draw.Size = new System.Drawing.Size(75, 23);
            this.button_draw.TabIndex = 8;
            this.button_draw.Text = "Draw";
            this.button_draw.UseVisualStyleBackColor = true;
            this.button_draw.Click += new System.EventHandler(this.button_draw_Click);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(12, 82);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(56, 13);
            this.label5.TabIndex = 9;
            this.label5.Text = "Rectangle";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(12, 178);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(27, 13);
            this.label6.TabIndex = 18;
            this.label6.Text = "Line";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(6, 226);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(21, 13);
            this.label7.TabIndex = 17;
            this.label7.Text = "px:";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(72, 226);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(21, 13);
            this.label8.TabIndex = 15;
            this.label8.Text = "px:";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(72, 200);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(15, 13);
            this.label9.TabIndex = 16;
            this.label9.Text = "y:";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(9, 200);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(15, 13);
            this.label10.TabIndex = 14;
            this.label10.Text = "x:";
            // 
            // line_py
            // 
            this.line_py.Location = new System.Drawing.Point(93, 223);
            this.line_py.Name = "line_py";
            this.line_py.Size = new System.Drawing.Size(40, 20);
            this.line_py.TabIndex = 13;
            // 
            // line_px
            // 
            this.line_px.Location = new System.Drawing.Point(25, 223);
            this.line_px.Name = "line_px";
            this.line_px.Size = new System.Drawing.Size(40, 20);
            this.line_px.TabIndex = 12;
            // 
            // line_y
            // 
            this.line_y.Location = new System.Drawing.Point(93, 197);
            this.line_y.Name = "line_y";
            this.line_y.Size = new System.Drawing.Size(40, 20);
            this.line_y.TabIndex = 11;
            // 
            // line_x
            // 
            this.line_x.Location = new System.Drawing.Point(25, 197);
            this.line_x.Name = "line_x";
            this.line_x.Size = new System.Drawing.Size(40, 20);
            this.line_x.TabIndex = 10;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(12, 270);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(33, 13);
            this.label11.TabIndex = 27;
            this.label11.Text = "Circle";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(6, 318);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(18, 13);
            this.label12.TabIndex = 26;
            this.label12.Text = "w:";
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(72, 318);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(16, 13);
            this.label13.TabIndex = 24;
            this.label13.Text = "h:";
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(72, 292);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(15, 13);
            this.label14.TabIndex = 25;
            this.label14.Text = "y:";
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Location = new System.Drawing.Point(9, 292);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(15, 13);
            this.label15.TabIndex = 23;
            this.label15.Text = "x:";
            // 
            // circle_h
            // 
            this.circle_h.Location = new System.Drawing.Point(93, 315);
            this.circle_h.Name = "circle_h";
            this.circle_h.Size = new System.Drawing.Size(40, 20);
            this.circle_h.TabIndex = 22;
            // 
            // circle_w
            // 
            this.circle_w.Location = new System.Drawing.Point(25, 315);
            this.circle_w.Name = "circle_w";
            this.circle_w.Size = new System.Drawing.Size(40, 20);
            this.circle_w.TabIndex = 21;
            // 
            // circle_y
            // 
            this.circle_y.Location = new System.Drawing.Point(93, 289);
            this.circle_y.Name = "circle_y";
            this.circle_y.Size = new System.Drawing.Size(40, 20);
            this.circle_y.TabIndex = 20;
            // 
            // circle_x
            // 
            this.circle_x.Location = new System.Drawing.Point(25, 289);
            this.circle_x.Name = "circle_x";
            this.circle_x.Size = new System.Drawing.Size(40, 20);
            this.circle_x.TabIndex = 19;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(894, 654);
            this.Controls.Add(this.label11);
            this.Controls.Add(this.label12);
            this.Controls.Add(this.label13);
            this.Controls.Add(this.label14);
            this.Controls.Add(this.label15);
            this.Controls.Add(this.circle_h);
            this.Controls.Add(this.circle_w);
            this.Controls.Add(this.circle_y);
            this.Controls.Add(this.circle_x);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.label10);
            this.Controls.Add(this.line_py);
            this.Controls.Add(this.line_px);
            this.Controls.Add(this.line_y);
            this.Controls.Add(this.line_x);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.button_draw);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.rect_h);
            this.Controls.Add(this.rect_w);
            this.Controls.Add(this.rect_y);
            this.Controls.Add(this.rect_x);
            this.Controls.Add(this.comboBox1);
            this.Name = "Form1";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.Paint += new System.Windows.Forms.PaintEventHandler(this.Form1_Paint);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ComboBox comboBox1;
        private System.Windows.Forms.TextBox rect_x;
        private System.Windows.Forms.TextBox rect_y;
        private System.Windows.Forms.TextBox rect_w;
        private System.Windows.Forms.TextBox rect_h;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Button button_draw;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.TextBox line_py;
        private System.Windows.Forms.TextBox line_px;
        private System.Windows.Forms.TextBox line_y;
        private System.Windows.Forms.TextBox line_x;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.TextBox circle_h;
        private System.Windows.Forms.TextBox circle_w;
        private System.Windows.Forms.TextBox circle_y;
        private System.Windows.Forms.TextBox circle_x;
    }
}

