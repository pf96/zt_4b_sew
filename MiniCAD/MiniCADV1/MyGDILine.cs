﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using GraphObjects;
using System.Drawing;

namespace MiniCADV1
{
    class MyGDILine:MyLine, IDrawable
    {
        public MyGDILine(int x, int y, int px, int py /*, string stroke, string fill, string color*/)
            :base(x, y, px, py /*, stroke, fill, color*/)
        { 
        }

        public void Draw(Graphics g)
        {
            g.DrawLine(new Pen(Color.Orange, 1), x, y, px, py);
        }
    }
}
