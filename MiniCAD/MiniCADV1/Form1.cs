﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
//
namespace MiniCADV1
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        MyGDIRect r = new MyGDIRect(0, 0, 0, 0);
        MyGDILine l = new MyGDILine(0, 0, 0, 0);
        MyGDICircle c = new MyGDICircle(0, 0, 0, 0);

        private void Form1_Load(object sender, EventArgs e)
        {
           
        }

        private void Form1_Paint(object sender, PaintEventArgs e)
        {
            //Graphics g = e.Graphics;

            //g.DrawLine(new Pen(Color.Orange, 2), 100, 10, 140, 80);
        }

        private void button_draw_Click(object sender, EventArgs e)
        {
            Graphics g = this.CreateGraphics();

            try
            {
                if (comboBox1.SelectedItem == "Rectangle")
                {
                    r = new MyGDIRect(int.Parse(rect_x.Text), int.Parse(rect_y.Text), int.Parse(rect_w.Text), int.Parse(rect_h.Text));
                    r.Draw(g);                    
                }               
                
                if (comboBox1.SelectedItem == "Line")
                {
                    l = new MyGDILine(int.Parse(line_x.Text), int.Parse(line_y.Text), int.Parse(line_px.Text), int.Parse(line_py.Text));
                    l.Draw(g);
                }                
                    
                if (comboBox1.SelectedItem == "Circle")
                {
                    c = new MyGDICircle(int.Parse(circle_x.Text), int.Parse(circle_y.Text), int.Parse(circle_w.Text), int.Parse(circle_h.Text));
                    c.Draw(g);
                }
                else
                    MessageBox.Show("Nothing selected!");
            }
            catch (Exception f)
            {
                MessageBox.Show("Something went wrong!" + Environment.NewLine + Environment.NewLine + f);
            }


        }
    }
}
