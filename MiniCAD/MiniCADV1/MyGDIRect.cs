﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using GraphObjects;
using System.Drawing;

namespace MiniCADV1
{
    class MyGDIRect:MyRect, IDrawable
    {
        public MyGDIRect(int x, int y, int w, int h /*, string stroke, string fill, string color*/)
            :base(x, y, w, h /*, stroke, fill, color*/)
        { 
        }
        public void Draw(Graphics g)
        {
            g.DrawRectangle(new Pen(Color.Red, 1), x, y, w, h);
        }
    }
}
