﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using GraphObjects;
using System.Drawing;

namespace MiniCADV1
{
    class MyGDICircle:MyCircle, IDrawable
    {
        public MyGDICircle(int x, int y, int r, int h /*, string stroke, string fill, string color*/)
            : base(x, y, r, h /*, stroke, fill, color*/)
        {
        }

        public void Draw(Graphics g)
        {
            g.DrawEllipse(new Pen(Color.Green, 1), x, y, r, h); ////////////////////////////////////////////////////
        }
    }
}
