﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GraphObjects
{
    public class MyCircle:GraphObject
    {
        protected int r;
        protected int h;
        public MyCircle(int x, int y, int r, int h/*, string stroke, string fill, string color*/)
        {
            this.x = x;
            this.y = y;
            this.r = r;
            this.h = h;
            //this.stroke = stroke;
            //this.fill = fill;
            //this.color = color;
        }
    }
}
