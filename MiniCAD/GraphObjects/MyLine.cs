﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GraphObjects
{
    public class MyLine:GraphObject
    {
        protected int px, py;
        public MyLine(int x, int y, int px, int py /*, string stroke, string fill, string color*/)
        {
            this.x = x;
            this.y = y;
            this.px = px;
            this.py = py;
            //this.stroke = stroke;
            //this.fill = fill;
            //this.color = color;
        }
    }
}
