﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GraphObjects
{
    public class MyRect:GraphObject
    {
        protected int w, h;
        public MyRect(int x, int y, int w, int h /*, string stroke, string fill, string color*/)
        {
            this.x = x;
            this.y = y;
            this.w = w;
            this.h = h;
            //this.stroke = stroke;
            //this.fill = fill;
            //this.color = color;
        }
    }
}
