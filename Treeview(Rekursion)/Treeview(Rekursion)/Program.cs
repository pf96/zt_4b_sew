﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Treeview_Rekursion_
{
    static class Program
    {
        static void Main(string[] args)
        {
            string pfad = @"D:\\Datein";

            Console.WriteLine(TreeStringBuilder(pfad));
        }

        static string TreeStringBuilder(string dir)
        {
            StringBuilder sb = new StringBuilder();
            MakeTree(dir, 0, sb);
            return sb.ToString();
        }

        static void MakeTree(string dir, int tiefe, StringBuilder sb)
        {
            sb.Append(new string(' ', tiefe));
            sb.Append(dir.Ende());
            sb.Append('\n');
            try
            {
                foreach (var item in Directory.GetDirectories(dir))
                    MakeTree(item, tiefe+1, sb);
            }
            catch(Exception ex)
            {
                Console.WriteLine(ex);
            }
        }

        public static string Ende(this string path, bool replaceSpaces = false)
        {
            string[] s = path.Split('\\');
            if (replaceSpaces) 
                return s[s.Length - 1].Replace(' ', '_');
            return s[s.Length - 1];
        }
    }        
}
