﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Net.Sockets;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace BessererClient
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {

        }

        private void button1_Click(object sender, EventArgs e)
        {
            TcpClient client = new TcpClient("localhost", 4711);
            try
            {
                Stream s = client.GetStream();
                s.ReadTimeout = 100;
                StreamReader sr = new StreamReader(s);

                StreamWriter sw = new StreamWriter(s);
                sw.AutoFlush = true;

                Console.WriteLine("schicke: " + textBox1.Text);
                sw.WriteLine(textBox1.Text);
                Console.WriteLine("gesendet!");

                string antwort = sr.ReadLine();
                this.textBox2.Text = antwort;

                //Console.ReadKey();
                s.Close();
            }
            finally
            {
                // code in finally block is guranteed 
                // to execute irrespective of 
                // whether any exception occurs or does 
                // not occur in the try block
                client.Close();
            }
        }
    }
}
