﻿#define LOG
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Sockets;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace EinfacherServer
{
    class Program
    {
        static TcpListener listener;
        const int LIMIT = 5; //5 concurrent clients

        public static void Main()
        {
            listener = new TcpListener(4711);
            listener.Start();
#if LOG
            Console.WriteLine("Server mounted, listening to port 4711");
#endif
            for (int i = 0; i < LIMIT; i++)
            {
                Thread t = new Thread(new ThreadStart(Service));
                t.Start();
            }
        }
        public static void Service()
        {
            while (true)
            {
                Socket soc = listener.AcceptSocket();
                //soc.SetSocketOption(SocketOptionLevel.Socket,
                //        SocketOptionName.ReceiveTimeout,10000);
#if LOG
                Console.WriteLine("Connected: {0}", 
                                         soc.RemoteEndPoint);
#endif
                try
                {
                    Stream s = new NetworkStream(soc);
                    StreamReader sr = new StreamReader(s);
                    StreamWriter sw = new StreamWriter(s);
                    sw.AutoFlush = true; // enable automatic flushing

                    string file = sr.ReadLine();

                    if (File.Exists(file))
                    {
                        Console.WriteLine("Request: " + file);
                        sw.WriteLine(File.ReadAllText(file));
                    }
                    else
                    {
                        sw.WriteLine(file + " not found");
                    }
                    sw.Flush();

                    s.Close();
                }
                catch (Exception e)
                {
#if LOG
                    Console.WriteLine(e.Message);
#endif
                }
#if LOG
                Console.WriteLine("Disconnected: {0}", 
                                        soc.RemoteEndPoint);
#endif
                soc.Close();
            }
        }
    }
}
