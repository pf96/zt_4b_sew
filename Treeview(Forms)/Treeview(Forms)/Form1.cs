﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Xml;

namespace Treeview_Forms_
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        static string PATH = @"C:\Users\Paul\Desktop";
        static XmlDocument xd = new XmlDocument();
        private void Form1_Load(object sender, EventArgs e)
        {
            //schöner mit OpenFileDialog den Startpfad machen
            TreeNode root = treeView1.Nodes.Add("Desktop");
            root.Tag = PATH;
            MakeTree(PATH, root);

            
        }

        private void MakeTree(string path, TreeNode current)
        {
            //Hilfsaufruf DirectoryInfo
            DirectoryInfo di = new DirectoryInfo(path);

            //Für alle Unterverzeichnisse des aktuellen Knotens
            foreach(var item in di.GetDirectories())
            {
                //Erzeuge neuen TreeNode
                TreeNode added = current.Nodes.Add(item.Name);
                added.Tag = item.FullName; 

                //und rufe Programm rekursiv auf
                MakeTree(item.FullName, added);
            }
        }

        private void treeView1_NodeMouseClick(object sender, TreeNodeMouseClickEventArgs e)
        {
            listView1.Items.Clear();
            DirectoryInfo di = new DirectoryInfo(e.Node.Tag.ToString());

            try
            {
                foreach (var item in di.GetFiles())
                {
                    string[] s = new string[5]{
                        item.Name,
                        item.LastWriteTimeUtc.ToString(),
                        item.Length.ToString(),
                        item.CreationTime.ToString(),
                        item.LastAccessTimeUtc.ToString()
                    };
                    listView1.Items.Add(new ListViewItem(s));
                }
            }
            catch
            {

            }
        }

        void MakeXML(string path, XmlNode current)
        {
            //Hilfsaufruf DirectoryInfo
            DirectoryInfo di = new DirectoryInfo(path);

            //Für alle Unterverzeichnisse des aktuellen Knotens
            foreach (var item in di.GetDirectories())
            {
                try
                {
                    //Erzeuge neuen TreeNode
                    XmlNode neu = xd.CreateNode("element", item.Name, "");
                    current.AppendChild(neu);

                    //und rufe Programm rekursiv auf
                    MakeXML(item.FullName, neu);
                }
                catch (Exception)
                {
                    //throw; 
                }
            }
        }

        private void savexml_Click(object sender, EventArgs e)
        {
            //Wurzel erstellen
            xd.LoadXml("<Desktop/>");
            XmlNode node = xd.SelectSingleNode("//Desktop");//XPath

            //XmlNode neu = xd.CreateNode("element", "e.Node.Tag.ToString()", "");
            //node.AppendChild(neu);

            MakeXML(PATH, node);
            xd.Save("XmlDirectories.xml");
        }        

        private void treeView1_AfterSelect(object sender, TreeViewEventArgs e) 
        {
            //listView1.Items.Clear();
            //DirectoryInfo di = new DirectoryInfo(e.Node.Tag.ToString());

            //try
            //{
            //    foreach (var item in di.GetFiles())
            //    {
            //        string[] s = new string[5]{
            //            item.Name,
            //            item.LastWriteTimeUtc.ToString(),
            //            item.Length.ToString(),
            //            item.CreationTime.ToString(),
            //            item.LastAccessTimeUtc.ToString()
            //        };
            //        listView1.Items.Add(new ListViewItem(s));
            //    }
            //}
            //catch
            //{

            //}
        }

        private void listView1_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        
    }
}
