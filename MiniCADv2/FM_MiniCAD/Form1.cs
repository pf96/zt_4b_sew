﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace MiniCADv2
{
    public partial class Form1 : Form
    {
        string select;
        List<CADObject> cadlist;
        int ex, ey;
        bool zoom;
        CADObject actobj;
        bool sel;
        Color co;
        int width;
        int zoomf = 1;

        public Form1()
        {
            InitializeComponent();
            cadlist = new List<CADObject>();
            sel = false;
            GB_CAW.Enabled = false;
        }


        private void bt_zoomit_Click_1(object sender, EventArgs e)
        {
            zoom = true;
            zoomf = zoomf * 2;
            PN_Draw.Invalidate();
        }

        private void bt_zoomout_Click(object sender, EventArgs e)
        {
            zoom = false;
            if (zoomf > 1)
            {
                zoomf = zoomf / 2;
            }
            else
            {
                MessageBox.Show("Kein Zoom mehr möglich!");
            }
            PN_Draw.Invalidate();
        }

        private void LB_Forms_SelectedIndexChanged_1(object sender, EventArgs e)
        {
            panel1.BackColor = Color.Transparent;
            textBox1.Text = "";
            co = Color.Transparent;
            width = 0;
            btn_draw.Enabled = true;
            bt_delete.Enabled = true;
            bt_zoomit.Enabled = true;
            bt_zoomout.Enabled = true;

            select = LB_Forms.SelectedItem.ToString();

            foreach (Control c in Controls)
            {
                if (c.GetType() == typeof(GroupBox))
                {
                    c.Enabled = false;
                }
            }

            if (LB_Forms.SelectedItem.ToString() == "Line")
                GB_line.Enabled = true;
            if (LB_Forms.SelectedItem.ToString() == "Circle")
                GB_circle.Enabled = true;
            if (LB_Forms.SelectedItem.ToString() == "Rectangle")
                GB_rectangle.Enabled = true;

            GB_CAW.Enabled = true;
        }

        private void panel1_MouseClick(object sender, MouseEventArgs e)
        {
            ColorDialog.ShowDialog();
            panel1.BackColor = ColorDialog.Color;
            co = ColorDialog.Color;
        }

        private void btn_draw_Click_1(object sender, EventArgs e)
        {
            {
                int cnt = 0;
                width = Int32.Parse(textBox1.Text);
                if (sel == true)
                {
                    cadlist.Remove(actobj);
                }
                #region circle
                if (GB_circle.Enabled == true)
                {
                    int x = Int32.Parse(tb_cx1.Text);
                    int y = Int32.Parse(tb_cy1.Text);
                    int rad = Int32.Parse(tb_crad.Text);

                    foreach (Control con in GB_circle.Controls)
                    {
                        if (con.GetType() == typeof(TextBox))
                        {
                            if ((y + rad) > PN_Draw.Height || (x + rad) > PN_Draw.Width)
                            {
                                MessageBox.Show("Zu groß!");
                                cnt = 1;
                                break;
                            }
                        }
                    }
                    if (cnt == 0)
                    {
                        Circle c = new Circle(co, width, x, x, rad);
                        cadlist.Add(c);
                    }
                    cnt = 0;
                    foreach (Control c in GB_circle.Controls)
                    {
                        if (c.GetType() == typeof(TextBox))
                            c.Text = "";
                    }
                }
                #endregion

                #region line
                if (GB_line.Enabled == true)
                {
                    foreach (Control con in GB_line.Controls)
                    {
                        if (con.GetType() == typeof(TextBox))
                        {
                            if (Int32.Parse(con.Text) > PN_Draw.Height || Int32.Parse(con.Text) > PN_Draw.Width)
                            {
                                MessageBox.Show("Zu groß!");
                                cnt = 1;
                                break;
                            }
                        }
                    }
                    if (cnt == 0)
                    {
                        Line l = new Line(co, width, Int32.Parse(tb_lx1.Text), Int32.Parse(tb_lx2.Text), Int32.Parse(tb_ly1.Text), Int32.Parse(tb_ly2.Text));
                        cadlist.Add(l);
                    }
                    cnt = 0;
                    foreach (Control c in GB_line.Controls)
                    {
                        if (c.GetType() == typeof(TextBox))
                            c.Text = "";
                    }
                }
                #endregion

                #region rectangle
                if (GB_rectangle.Enabled == true)
                {
                    int rx = Int32.Parse(tb_rx1.Text);
                    int ry = Int32.Parse(tb_ry1.Text);
                    int h = Int32.Parse(tb_rh.Text);
                    int w = Int32.Parse(tb_rw.Text);

                    foreach (Control con in GB_rectangle.Controls)
                    {
                        if (con.GetType() == typeof(TextBox))
                        {
                            if ((ry + h) > PN_Draw.Height || (rx + w) > PN_Draw.Width)
                            {
                                MessageBox.Show("Zu groß!");
                                cnt = 1;
                                break;
                            }
                        }
                    }
                    if (cnt == 0)
                    {
                        Rectangle r = new Rectangle(co, width, rx, ry, h, w);
                        cadlist.Add(r);
                    }
                    cnt = 0;
                    foreach (Control c in GB_rectangle.Controls)
                    {
                        if (c.GetType() == typeof(TextBox))
                            c.Text = "";
                    }
                }
                #endregion

                PN_Draw.Invalidate();
            }
           
        }

        private void bt_delete_Click_1(object sender, EventArgs e)
        {
            cadlist.Remove(actobj);

            if (actobj.GetType() == typeof(Rectangle))
            {
                this.tb_rx1.Text = "";
                this.tb_ry1.Text = "";
                this.tb_rw.Text = "";
                this.tb_rh.Text = "";
            }
            if (actobj.GetType() == typeof(Circle))
            {
                this.tb_cx1.Text = "";
                this.tb_cy1.Text = "";
                this.tb_crad.Text = "";
            }
            if (actobj.GetType() == typeof(Line))
            {
                this.tb_lx1.Text = "";
                this.tb_lx2.Text = "";
                this.tb_ly1.Text = "";
                this.tb_ly2.Text = "";

            }
            PN_Draw.Invalidate();
        }

        private void PN_Draw_Paint_1(object sender, PaintEventArgs e)
        {
            if (zoom == true)
            {
                e.Graphics.Transform = new System.Drawing.Drawing2D.Matrix(zoomf, 0, 0, zoomf, 0, 0);
            }
            if (zoom == false)
            {
                e.Graphics.Transform = new System.Drawing.Drawing2D.Matrix(zoomf, 0, 0, zoomf, 0, 0);
            }
            if (cadlist == null) { }
            else
            {
                foreach (CADObject obj in cadlist)
                {
                    obj.draw(e.Graphics);
                    if (obj.amIselected(ex, ey) == true)
                    {
                        sel = true;
                        obj.selected(e.Graphics);
                        actobj = obj;
                        if (obj.GetType() == typeof(Rectangle))
                        {
                            Rectangle r = (Rectangle)obj;
                            this.tb_rx1.Text = r.x.ToString();
                            this.tb_ry1.Text = r.y.ToString();
                            this.tb_rw.Text = r.w.ToString();
                            this.tb_rh.Text = r.h.ToString();
                            this.GB_rectangle.Enabled = true;
                            this.GB_line.Enabled = false;
                            this.GB_circle.Enabled = false;
                        }
                        if (obj.GetType() == typeof(Circle))
                        {
                            Circle c = (Circle)obj;
                            this.tb_cx1.Text = c.x1.ToString();
                            this.tb_cy1.Text = c.y1.ToString();
                            this.tb_crad.Text = c.rad.ToString();
                            this.GB_rectangle.Enabled = false;
                            this.GB_line.Enabled = false;
                            this.GB_circle.Enabled = true;
                        }
                        if (obj.GetType() == typeof(Line))
                        {
                            Line l = (Line)obj;
                            this.tb_lx1.Text = l.x1.ToString();
                            this.tb_lx2.Text = l.x2.ToString();
                            this.tb_ly1.Text = l.y1.ToString();
                            this.tb_ly2.Text = l.y2.ToString();
                            this.GB_rectangle.Enabled = false;
                            this.GB_line.Enabled = true;
                            this.GB_circle.Enabled = false;
                        }
                    }
                    else
                    {
                        if (obj.GetType() == typeof(Rectangle))
                        {
                            this.tb_rx1.Text = "";
                            this.tb_ry1.Text = "";
                            this.tb_rw.Text = "";
                            this.tb_rh.Text = "";
                            actobj = null;
                        }
                        if (obj.GetType() == typeof(Circle))
                        {
                            this.tb_cx1.Text = "";
                            this.tb_cy1.Text = "";
                            this.tb_crad.Text = "";
                            actobj = null;
                        }
                        if (obj.GetType() == typeof(Line))
                        {
                            this.tb_lx1.Text = "";
                            this.tb_lx2.Text = "";
                            this.tb_ly1.Text = "";
                            this.tb_ly2.Text = "";
                            actobj = null;
                        }
                    }

                }
            }
        }

        private void PN_Draw_MouseClick(object sender, MouseEventArgs e)
        {
            ex = e.X;
            ey = e.Y;
            PN_Draw.Invalidate(); //raises the paintevent. all paint events are called
        }

        private void label10_Click(object sender, EventArgs e)
        {

        }

        private void Form1_Load(object sender, EventArgs e)
        {

        }


    }
}
