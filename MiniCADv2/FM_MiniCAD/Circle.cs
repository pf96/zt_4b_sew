﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;

namespace MiniCADv2
{
    class Circle : CADObject
    {
        public float x1, y1;
        public int rad;
        public Color c;
        public float width;

        public Circle(Color c, float width, float x1, float y1, int rad)
        {
            this.c = c;
            this.width = width;
            this.x1 = x1;
            this.y1 = y1;
            this.rad = rad;
        }
        public override void draw(Graphics e)
        {
            e.DrawEllipse(new Pen(c, width), x1 - rad, y1 - rad, rad * 2, rad * 2);
        }
        public override bool amIselected(int nx, int ny)
        {
            if (nx >= x1 - rad && nx <= (x1 + rad))
            {
                if (ny >= y1 - rad && ny <= (y1 + rad))
                    return true;
                else
                    return false;
            }
            else
            {
                return false;
            }
        }

        public override void selected(Graphics e)
        {
            e.DrawRectangle(new Pen(Color.Red), x1 - (rad + 5), y1, 5, 5);
            e.DrawRectangle(new Pen(Color.Red), x1, y1 - (rad + 5), 5, 5);
            e.DrawRectangle(new Pen(Color.Red), x1 + rad, y1, 5, 5);
            e.DrawRectangle(new Pen(Color.Red), x1, y1 + rad, 5, 5);
        }
    }
}
