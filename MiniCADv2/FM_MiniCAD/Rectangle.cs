﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;

namespace MiniCADv2
{
    class Rectangle : CADObject
    {
        public float x;
        public float y;
        public float h;
        public float w;
        public Color c;
        public float width;

        public Rectangle(Color c, int width, int x, int y, float h, float w)
        {
            this.c = c;
            this.width = width;
            this.x = x;
            this.y = y;
            this.h = h;
            this.w = w;
        }

        public override void draw(Graphics e)
        {
            e.DrawRectangle(new Pen(c, width), x, y, w, h);
        }

        public override bool amIselected(int nx, int ny)
        {
            if (nx >= x && nx <= (x + w))
            {
                if (ny >= y && ny <= (y + h))
                    return true;
                else
                    return false;
            }
            else
            {
                return false;
            }
        }

        public override void selected(Graphics e)
        {
            e.DrawRectangle(new Pen(Color.Red), x - 5, y - 5, 5, 5);
            e.DrawRectangle(new Pen(Color.Red), (x + w), (y - 5), 5, 5);
            e.DrawRectangle(new Pen(Color.Red), (x - 5), (y + h), 5, 5);
            e.DrawRectangle(new Pen(Color.Red), (x + w), (y + h), 5, 5);
        }
    }
}
