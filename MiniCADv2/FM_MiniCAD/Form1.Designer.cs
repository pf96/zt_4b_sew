﻿namespace MiniCADv2
{
    partial class Form1
    {
        /// <summary>
        /// Erforderliche Designervariable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Verwendete Ressourcen bereinigen.
        /// </summary>
        /// <param name="disposing">True, wenn verwaltete Ressourcen gelöscht werden sollen; andernfalls False.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Vom Windows Form-Designer generierter Code

        /// <summary>
        /// Erforderliche Methode für die Designerunterstützung.
        /// Der Inhalt der Methode darf nicht mit dem Code-Editor geändert werden.
        /// </summary>
        private void InitializeComponent()
        {
            this.LB_Forms = new System.Windows.Forms.ListBox();
            this.GB_CAW = new System.Windows.Forms.GroupBox();
            this.panel1 = new System.Windows.Forms.Panel();
            this.label10 = new System.Windows.Forms.Label();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.label8 = new System.Windows.Forms.Label();
            this.GB_line = new System.Windows.Forms.GroupBox();
            this.label4 = new System.Windows.Forms.Label();
            this.tb_ly2 = new System.Windows.Forms.TextBox();
            this.tb_ly1 = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.tb_lx2 = new System.Windows.Forms.TextBox();
            this.tb_lx1 = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.GB_circle = new System.Windows.Forms.GroupBox();
            this.tb_crad = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.tb_cy1 = new System.Windows.Forms.TextBox();
            this.tb_cx1 = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.GB_rectangle = new System.Windows.Forms.GroupBox();
            this.tb_rw = new System.Windows.Forms.TextBox();
            this.tb_rh = new System.Windows.Forms.TextBox();
            this.label13 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.tb_rx1 = new System.Windows.Forms.TextBox();
            this.label11 = new System.Windows.Forms.Label();
            this.tb_ry1 = new System.Windows.Forms.TextBox();
            this.label9 = new System.Windows.Forms.Label();
            this.btn_draw = new System.Windows.Forms.Button();
            this.bt_delete = new System.Windows.Forms.Button();
            this.bt_zoomit = new System.Windows.Forms.Button();
            this.bt_zoomout = new System.Windows.Forms.Button();
            this.PN_Draw = new System.Windows.Forms.Panel();
            this.ColorDialog = new System.Windows.Forms.ColorDialog();
            this.bt_save = new System.Windows.Forms.Button();
            this.bt_load = new System.Windows.Forms.Button();
            this.GB_CAW.SuspendLayout();
            this.GB_line.SuspendLayout();
            this.GB_circle.SuspendLayout();
            this.GB_rectangle.SuspendLayout();
            this.SuspendLayout();
            // 
            // LB_Forms
            // 
            this.LB_Forms.FormattingEnabled = true;
            this.LB_Forms.Items.AddRange(new object[] {
            "Rectangle",
            "Circle",
            "Line"});
            this.LB_Forms.Location = new System.Drawing.Point(4, 12);
            this.LB_Forms.Name = "LB_Forms";
            this.LB_Forms.Size = new System.Drawing.Size(200, 82);
            this.LB_Forms.TabIndex = 12;
            this.LB_Forms.SelectedIndexChanged += new System.EventHandler(this.LB_Forms_SelectedIndexChanged_1);
            // 
            // GB_CAW
            // 
            this.GB_CAW.Controls.Add(this.panel1);
            this.GB_CAW.Controls.Add(this.label10);
            this.GB_CAW.Controls.Add(this.textBox1);
            this.GB_CAW.Controls.Add(this.label8);
            this.GB_CAW.Location = new System.Drawing.Point(4, 100);
            this.GB_CAW.Name = "GB_CAW";
            this.GB_CAW.Size = new System.Drawing.Size(200, 100);
            this.GB_CAW.TabIndex = 13;
            this.GB_CAW.TabStop = false;
            this.GB_CAW.Text = "Color/Width";
            // 
            // panel1
            // 
            this.panel1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel1.Location = new System.Drawing.Point(50, 49);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(57, 22);
            this.panel1.TabIndex = 6;
            this.panel1.MouseClick += new System.Windows.Forms.MouseEventHandler(this.panel1_MouseClick);
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(7, 56);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(40, 13);
            this.label10.TabIndex = 4;
            this.label10.Text = "Colour:";
            this.label10.Click += new System.EventHandler(this.label10_Click);
            // 
            // textBox1
            // 
            this.textBox1.Location = new System.Drawing.Point(50, 23);
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new System.Drawing.Size(57, 20);
            this.textBox1.TabIndex = 3;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(6, 26);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(38, 13);
            this.label8.TabIndex = 0;
            this.label8.Text = "Width:";
            // 
            // GB_line
            // 
            this.GB_line.Controls.Add(this.label4);
            this.GB_line.Controls.Add(this.tb_ly2);
            this.GB_line.Controls.Add(this.tb_ly1);
            this.GB_line.Controls.Add(this.label3);
            this.GB_line.Controls.Add(this.tb_lx2);
            this.GB_line.Controls.Add(this.tb_lx1);
            this.GB_line.Controls.Add(this.label2);
            this.GB_line.Controls.Add(this.label1);
            this.GB_line.Enabled = false;
            this.GB_line.Location = new System.Drawing.Point(4, 419);
            this.GB_line.Name = "GB_line";
            this.GB_line.Size = new System.Drawing.Size(200, 76);
            this.GB_line.TabIndex = 14;
            this.GB_line.TabStop = false;
            this.GB_line.Text = "Line";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(106, 46);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(21, 13);
            this.label4.TabIndex = 7;
            this.label4.Text = "y2:";
            // 
            // tb_ly2
            // 
            this.tb_ly2.Location = new System.Drawing.Point(133, 43);
            this.tb_ly2.Name = "tb_ly2";
            this.tb_ly2.Size = new System.Drawing.Size(57, 20);
            this.tb_ly2.TabIndex = 6;
            // 
            // tb_ly1
            // 
            this.tb_ly1.Location = new System.Drawing.Point(133, 17);
            this.tb_ly1.Name = "tb_ly1";
            this.tb_ly1.Size = new System.Drawing.Size(57, 20);
            this.tb_ly1.TabIndex = 5;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(106, 20);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(21, 13);
            this.label3.TabIndex = 4;
            this.label3.Text = "y1:";
            // 
            // tb_lx2
            // 
            this.tb_lx2.Location = new System.Drawing.Point(34, 43);
            this.tb_lx2.Name = "tb_lx2";
            this.tb_lx2.Size = new System.Drawing.Size(57, 20);
            this.tb_lx2.TabIndex = 3;
            // 
            // tb_lx1
            // 
            this.tb_lx1.Location = new System.Drawing.Point(34, 17);
            this.tb_lx1.Name = "tb_lx1";
            this.tb_lx1.Size = new System.Drawing.Size(57, 20);
            this.tb_lx1.TabIndex = 2;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(7, 46);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(21, 13);
            this.label2.TabIndex = 1;
            this.label2.Text = "x2:";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(6, 20);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(21, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "x1:";
            // 
            // GB_circle
            // 
            this.GB_circle.Controls.Add(this.tb_crad);
            this.GB_circle.Controls.Add(this.label7);
            this.GB_circle.Controls.Add(this.label6);
            this.GB_circle.Controls.Add(this.tb_cy1);
            this.GB_circle.Controls.Add(this.tb_cx1);
            this.GB_circle.Controls.Add(this.label5);
            this.GB_circle.Enabled = false;
            this.GB_circle.Location = new System.Drawing.Point(4, 313);
            this.GB_circle.Name = "GB_circle";
            this.GB_circle.Size = new System.Drawing.Size(200, 100);
            this.GB_circle.TabIndex = 15;
            this.GB_circle.TabStop = false;
            this.GB_circle.Text = "Circle";
            // 
            // tb_crad
            // 
            this.tb_crad.Location = new System.Drawing.Point(55, 43);
            this.tb_crad.Name = "tb_crad";
            this.tb_crad.Size = new System.Drawing.Size(57, 20);
            this.tb_crad.TabIndex = 8;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(6, 46);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(43, 13);
            this.label7.TabIndex = 7;
            this.label7.Text = "Radius:";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(106, 20);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(21, 13);
            this.label6.TabIndex = 6;
            this.label6.Text = "y1:";
            // 
            // tb_cy1
            // 
            this.tb_cy1.Location = new System.Drawing.Point(133, 17);
            this.tb_cy1.Name = "tb_cy1";
            this.tb_cy1.Size = new System.Drawing.Size(57, 20);
            this.tb_cy1.TabIndex = 5;
            // 
            // tb_cx1
            // 
            this.tb_cx1.Location = new System.Drawing.Point(34, 17);
            this.tb_cx1.Name = "tb_cx1";
            this.tb_cx1.Size = new System.Drawing.Size(57, 20);
            this.tb_cx1.TabIndex = 4;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(7, 20);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(21, 13);
            this.label5.TabIndex = 0;
            this.label5.Text = "x1:";
            // 
            // GB_rectangle
            // 
            this.GB_rectangle.Controls.Add(this.tb_rw);
            this.GB_rectangle.Controls.Add(this.tb_rh);
            this.GB_rectangle.Controls.Add(this.label13);
            this.GB_rectangle.Controls.Add(this.label12);
            this.GB_rectangle.Controls.Add(this.tb_rx1);
            this.GB_rectangle.Controls.Add(this.label11);
            this.GB_rectangle.Controls.Add(this.tb_ry1);
            this.GB_rectangle.Controls.Add(this.label9);
            this.GB_rectangle.Enabled = false;
            this.GB_rectangle.Location = new System.Drawing.Point(4, 207);
            this.GB_rectangle.Name = "GB_rectangle";
            this.GB_rectangle.Size = new System.Drawing.Size(200, 100);
            this.GB_rectangle.TabIndex = 16;
            this.GB_rectangle.TabStop = false;
            this.GB_rectangle.Text = "Rectangle";
            // 
            // tb_rw
            // 
            this.tb_rw.Location = new System.Drawing.Point(53, 71);
            this.tb_rw.Name = "tb_rw";
            this.tb_rw.Size = new System.Drawing.Size(57, 20);
            this.tb_rw.TabIndex = 19;
            // 
            // tb_rh
            // 
            this.tb_rh.Location = new System.Drawing.Point(53, 48);
            this.tb_rh.Name = "tb_rh";
            this.tb_rh.Size = new System.Drawing.Size(57, 20);
            this.tb_rh.TabIndex = 18;
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(6, 51);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(41, 13);
            this.label13.TabIndex = 17;
            this.label13.Text = "Height:";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(6, 74);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(38, 13);
            this.label12.TabIndex = 16;
            this.label12.Text = "Width:";
            // 
            // tb_rx1
            // 
            this.tb_rx1.Location = new System.Drawing.Point(34, 19);
            this.tb_rx1.Name = "tb_rx1";
            this.tb_rx1.Size = new System.Drawing.Size(57, 20);
            this.tb_rx1.TabIndex = 10;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(6, 22);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(21, 13);
            this.label11.TabIndex = 8;
            this.label11.Text = "x1:";
            // 
            // tb_ry1
            // 
            this.tb_ry1.Location = new System.Drawing.Point(133, 19);
            this.tb_ry1.Name = "tb_ry1";
            this.tb_ry1.Size = new System.Drawing.Size(57, 20);
            this.tb_ry1.TabIndex = 13;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(106, 22);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(21, 13);
            this.label9.TabIndex = 12;
            this.label9.Text = "y1:";
            // 
            // btn_draw
            // 
            this.btn_draw.Enabled = false;
            this.btn_draw.Location = new System.Drawing.Point(4, 501);
            this.btn_draw.Name = "btn_draw";
            this.btn_draw.Size = new System.Drawing.Size(90, 31);
            this.btn_draw.TabIndex = 17;
            this.btn_draw.Text = "Draw";
            this.btn_draw.UseVisualStyleBackColor = true;
            this.btn_draw.Click += new System.EventHandler(this.btn_draw_Click_1);
            // 
            // bt_delete
            // 
            this.bt_delete.Enabled = false;
            this.bt_delete.Location = new System.Drawing.Point(113, 501);
            this.bt_delete.Name = "bt_delete";
            this.bt_delete.Size = new System.Drawing.Size(90, 31);
            this.bt_delete.TabIndex = 18;
            this.bt_delete.Text = "Delete";
            this.bt_delete.UseVisualStyleBackColor = true;
            this.bt_delete.Click += new System.EventHandler(this.bt_delete_Click_1);
            // 
            // bt_zoomit
            // 
            this.bt_zoomit.Enabled = false;
            this.bt_zoomit.Location = new System.Drawing.Point(4, 538);
            this.bt_zoomit.Name = "bt_zoomit";
            this.bt_zoomit.Size = new System.Drawing.Size(92, 31);
            this.bt_zoomit.TabIndex = 19;
            this.bt_zoomit.Text = "Zoom in";
            this.bt_zoomit.UseVisualStyleBackColor = true;
            this.bt_zoomit.Click += new System.EventHandler(this.bt_zoomit_Click_1);
            // 
            // bt_zoomout
            // 
            this.bt_zoomout.Enabled = false;
            this.bt_zoomout.Location = new System.Drawing.Point(112, 538);
            this.bt_zoomout.Name = "bt_zoomout";
            this.bt_zoomout.Size = new System.Drawing.Size(91, 31);
            this.bt_zoomout.TabIndex = 20;
            this.bt_zoomout.Text = "Zoom out";
            this.bt_zoomout.UseVisualStyleBackColor = true;
            this.bt_zoomout.Click += new System.EventHandler(this.bt_zoomout_Click);
            // 
            // PN_Draw
            // 
            this.PN_Draw.BackColor = System.Drawing.Color.Silver;
            this.PN_Draw.Cursor = System.Windows.Forms.Cursors.Cross;
            this.PN_Draw.Location = new System.Drawing.Point(234, 12);
            this.PN_Draw.Name = "PN_Draw";
            this.PN_Draw.Size = new System.Drawing.Size(840, 594);
            this.PN_Draw.TabIndex = 21;
            this.PN_Draw.Paint += new System.Windows.Forms.PaintEventHandler(this.PN_Draw_Paint_1);
            this.PN_Draw.MouseClick += new System.Windows.Forms.MouseEventHandler(this.PN_Draw_MouseClick);
            // 
            // ColorDialog
            // 
            this.ColorDialog.Color = System.Drawing.Color.White;
            // 
            // bt_save
            // 
            this.bt_save.Location = new System.Drawing.Point(4, 575);
            this.bt_save.Name = "bt_save";
            this.bt_save.Size = new System.Drawing.Size(90, 31);
            this.bt_save.TabIndex = 22;
            this.bt_save.Text = "Save";
            this.bt_save.UseVisualStyleBackColor = true;
            // 
            // bt_load
            // 
            this.bt_load.Location = new System.Drawing.Point(112, 575);
            this.bt_load.Name = "bt_load";
            this.bt_load.Size = new System.Drawing.Size(90, 31);
            this.bt_load.TabIndex = 23;
            this.bt_load.Text = "Load";
            this.bt_load.UseVisualStyleBackColor = true;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1084, 615);
            this.Controls.Add(this.bt_load);
            this.Controls.Add(this.bt_save);
            this.Controls.Add(this.GB_rectangle);
            this.Controls.Add(this.PN_Draw);
            this.Controls.Add(this.bt_zoomout);
            this.Controls.Add(this.bt_zoomit);
            this.Controls.Add(this.bt_delete);
            this.Controls.Add(this.btn_draw);
            this.Controls.Add(this.GB_circle);
            this.Controls.Add(this.GB_line);
            this.Controls.Add(this.GB_CAW);
            this.Controls.Add(this.LB_Forms);
            this.Name = "Form1";
            this.Text = "Mini_CADv2";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.GB_CAW.ResumeLayout(false);
            this.GB_CAW.PerformLayout();
            this.GB_line.ResumeLayout(false);
            this.GB_line.PerformLayout();
            this.GB_circle.ResumeLayout(false);
            this.GB_circle.PerformLayout();
            this.GB_rectangle.ResumeLayout(false);
            this.GB_rectangle.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.ListBox LB_Forms;
        private System.Windows.Forms.GroupBox GB_CAW;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Label label10;
        public System.Windows.Forms.TextBox textBox1;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.GroupBox GB_line;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox tb_ly2;
        private System.Windows.Forms.TextBox tb_ly1;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox tb_lx2;
        private System.Windows.Forms.TextBox tb_lx1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.GroupBox GB_circle;
        private System.Windows.Forms.TextBox tb_crad;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox tb_cy1;
        private System.Windows.Forms.TextBox tb_cx1;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.GroupBox GB_rectangle;
        public System.Windows.Forms.TextBox tb_rw;
        public System.Windows.Forms.TextBox tb_rh;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label12;
        public System.Windows.Forms.TextBox tb_rx1;
        private System.Windows.Forms.Label label11;
        public System.Windows.Forms.TextBox tb_ry1;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Button btn_draw;
        private System.Windows.Forms.Button bt_delete;
        private System.Windows.Forms.Button bt_zoomit;
        private System.Windows.Forms.Button bt_zoomout;
        private System.Windows.Forms.Panel PN_Draw;
        private System.Windows.Forms.ColorDialog ColorDialog;
        private System.Windows.Forms.Button bt_save;
        private System.Windows.Forms.Button bt_load;
    }
}

