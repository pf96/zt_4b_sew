﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;

namespace MiniCADv2
{
    abstract class CADObject
    {
        public CADObject()
        {

        }
        public abstract void draw(Graphics e);
        public abstract bool amIselected(int x, int y);
        public abstract void selected(Graphics e);
    }
}
