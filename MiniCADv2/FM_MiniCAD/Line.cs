﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;

namespace MiniCADv2
{
    class Line : CADObject
    {
        public int x1;
        public int x2;
        public int y1;
        public int y2;
        public Color c;
        public int width;
        int type;

        public Line(Color c, int width, int x1, int x2, int y1, int y2)
        {
            this.c = c;
            this.width = width;
            this.x1 = x1;
            this.x2 = x2;
            this.y1 = y1;
            this.y2 = y2;
        }

        public override void draw(Graphics e)
        {
            e.DrawLine(new Pen(c, width), x1, y1, x2, y2);
        }

        public override bool amIselected(int x, int y)
        {
            if (x1 < x && x2 > x)
            {
                if (y1 < y && y2 > y)
                {
                    type = 1;
                    return true;
                }
            }
            if (x1 > x && x2 < x)
            {
                if (y1 < y && y2 > y)
                {
                    type = 2;
                    return true;
                }
            }
            return false;
        }

        public override void selected(Graphics e)
        {
            if (type == 1)
            {
                e.DrawRectangle(new Pen(Color.Red), x1 - 5, y1 - 5, 5, 5);
                e.DrawRectangle(new Pen(Color.Red), x2, y2, 5, 5);
            }
            if (type == 2)
            {
                e.DrawRectangle(new Pen(Color.Red), x1, y1 - 5, 5, 5);
                e.DrawRectangle(new Pen(Color.Red), x2 - 5, y2, 5, 5);
            }
        }
    }
}
