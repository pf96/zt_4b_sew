﻿namespace LINQ_Scott
{
    partial class Form1
    {
        /// <summary>
        /// Erforderliche Designervariable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Verwendete Ressourcen bereinigen.
        /// </summary>
        /// <param name="disposing">True, wenn verwaltete Ressourcen gelöscht werden sollen; andernfalls False.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Vom Windows Form-Designer generierter Code

        /// <summary>
        /// Erforderliche Methode für die Designerunterstützung.
        /// Der Inhalt der Methode darf nicht mit dem Code-Editor geändert werden.
        /// </summary>
        private void InitializeComponent()
        {
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.deptno_comboBox1 = new System.Windows.Forms.ComboBox();
            this.label1 = new System.Windows.Forms.Label();
            this.dname_textBox1 = new System.Windows.Forms.TextBox();
            this.loc_textBox2 = new System.Windows.Forms.TextBox();
            this.flowLayoutPanel1 = new System.Windows.Forms.FlowLayoutPanel();
            this.delete_btn = new System.Windows.Forms.Button();
            this.emp_add_btn = new System.Windows.Forms.Button();
            this.propertyGrid1 = new System.Windows.Forms.PropertyGrid();
            this.savechangebtn = new System.Windows.Forms.Button();
            this.emp_update_btn = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(20, 43);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(39, 13);
            this.label2.TabIndex = 2;
            this.label2.Text = "dname";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(203, 44);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(21, 13);
            this.label3.TabIndex = 3;
            this.label3.Text = "loc";
            // 
            // deptno_comboBox1
            // 
            this.deptno_comboBox1.FormattingEnabled = true;
            this.deptno_comboBox1.Location = new System.Drawing.Point(65, 14);
            this.deptno_comboBox1.Name = "deptno_comboBox1";
            this.deptno_comboBox1.Size = new System.Drawing.Size(121, 21);
            this.deptno_comboBox1.TabIndex = 0;
            this.deptno_comboBox1.SelectedIndexChanged += new System.EventHandler(this.deptno_comboBox1_SelectedIndexChanged);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(19, 17);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(40, 13);
            this.label1.TabIndex = 1;
            this.label1.Text = "deptno";
            // 
            // dname_textBox1
            // 
            this.dname_textBox1.Location = new System.Drawing.Point(65, 41);
            this.dname_textBox1.Name = "dname_textBox1";
            this.dname_textBox1.Size = new System.Drawing.Size(121, 20);
            this.dname_textBox1.TabIndex = 4;
            // 
            // loc_textBox2
            // 
            this.loc_textBox2.Location = new System.Drawing.Point(230, 40);
            this.loc_textBox2.Name = "loc_textBox2";
            this.loc_textBox2.Size = new System.Drawing.Size(121, 20);
            this.loc_textBox2.TabIndex = 5;
            // 
            // flowLayoutPanel1
            // 
            this.flowLayoutPanel1.Location = new System.Drawing.Point(22, 105);
            this.flowLayoutPanel1.Name = "flowLayoutPanel1";
            this.flowLayoutPanel1.Size = new System.Drawing.Size(393, 220);
            this.flowLayoutPanel1.TabIndex = 6;
            // 
            // delete_btn
            // 
            this.delete_btn.Location = new System.Drawing.Point(22, 66);
            this.delete_btn.Name = "delete_btn";
            this.delete_btn.Size = new System.Drawing.Size(75, 23);
            this.delete_btn.TabIndex = 7;
            this.delete_btn.Text = "Delete";
            this.delete_btn.UseVisualStyleBackColor = true;
            this.delete_btn.Click += new System.EventHandler(this.delete_btn_Click);
            // 
            // emp_add_btn
            // 
            this.emp_add_btn.Location = new System.Drawing.Point(103, 66);
            this.emp_add_btn.Name = "emp_add_btn";
            this.emp_add_btn.Size = new System.Drawing.Size(95, 23);
            this.emp_add_btn.TabIndex = 8;
            this.emp_add_btn.Text = "insert EMP";
            this.emp_add_btn.UseVisualStyleBackColor = true;
            this.emp_add_btn.Click += new System.EventHandler(this.emp_add_btn_Click);
            // 
            // propertyGrid1
            // 
            this.propertyGrid1.Location = new System.Drawing.Point(434, 54);
            this.propertyGrid1.Name = "propertyGrid1";
            this.propertyGrid1.Size = new System.Drawing.Size(369, 271);
            this.propertyGrid1.TabIndex = 9;
            // 
            // savechangebtn
            // 
            this.savechangebtn.Location = new System.Drawing.Point(692, 331);
            this.savechangebtn.Name = "savechangebtn";
            this.savechangebtn.Size = new System.Drawing.Size(111, 23);
            this.savechangebtn.TabIndex = 10;
            this.savechangebtn.Text = "save changes";
            this.savechangebtn.UseVisualStyleBackColor = true;
            this.savechangebtn.Click += new System.EventHandler(this.savechangebtn_Click);
            // 
            // emp_update_btn
            // 
            this.emp_update_btn.Location = new System.Drawing.Point(204, 66);
            this.emp_update_btn.Name = "emp_update_btn";
            this.emp_update_btn.Size = new System.Drawing.Size(95, 23);
            this.emp_update_btn.TabIndex = 11;
            this.emp_update_btn.Text = "update EMP";
            this.emp_update_btn.UseVisualStyleBackColor = true;
            this.emp_update_btn.Click += new System.EventHandler(this.emp_update_btn_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(824, 388);
            this.Controls.Add(this.emp_update_btn);
            this.Controls.Add(this.savechangebtn);
            this.Controls.Add(this.propertyGrid1);
            this.Controls.Add(this.emp_add_btn);
            this.Controls.Add(this.delete_btn);
            this.Controls.Add(this.flowLayoutPanel1);
            this.Controls.Add(this.loc_textBox2);
            this.Controls.Add(this.dname_textBox1);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.deptno_comboBox1);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Name = "Form1";
            this.Text = "Form1";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.ComboBox deptno_comboBox1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox dname_textBox1;
        private System.Windows.Forms.TextBox loc_textBox2;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel1;
        private System.Windows.Forms.Button delete_btn;
        private System.Windows.Forms.Button emp_add_btn;
        private System.Windows.Forms.PropertyGrid propertyGrid1;
        private System.Windows.Forms.Button savechangebtn;
        private System.Windows.Forms.Button emp_update_btn;

    }
}

