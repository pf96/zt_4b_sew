﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using SewContext;

namespace LINQ_Scott
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        SewContext.SewDataContext sdc = new SewDataContext();

        Emp selected;

        private void Form1_Load(object sender, EventArgs e)
        {
            //var depts = from d in sdc.Depts select d;

            foreach (var item in sdc.Depts) //geht auch direkt
            {
                deptno_comboBox1.Items.Add(item.DEPTNO);
            }
        }

        private void deptno_comboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
            //prevent errors
            if (deptno_comboBox1.SelectedItem.ToString() == "") return;
            //get Departments
            var currdep = from d in sdc.Depts
                          where d.DEPTNO == Convert.ToInt32(deptno_comboBox1.SelectedItem.ToString())
                          select d;
            //fill textboxes
            dname_textBox1.Text = currdep.FirstOrDefault().DNAME;
            loc_textBox2.Text = currdep.FirstOrDefault().LOC;

            //Get Employes
             var emps = from emp in sdc.Emps
                           where emp.DEPTNO == Convert.ToInt32(deptno_comboBox1.SelectedItem.ToString())
                           select emp;

            //clear FlowLayoutPanel
            flowLayoutPanel1.Controls.Clear();
            //fill FLP
            foreach (var item in emps)
            {
                Label l = new Label();
                l.Width = 600;
                l.Text = item.EMPNO + " - " + item.ENAME + " - " + item.DEPTNO;
                flowLayoutPanel1.Controls.Add(l);

                //1.label Tag setzen
                l.Tag = item;
                l.Click += l_Click;
            }
        }

        //NAch klick auf Label "Tag" auslesen und dem Prperty Grid übergeben
        void l_Click(object sender, EventArgs e)
        {
            //2. beim Klick den "sender" maerken als "selected"
            Label l = (Label)sender;

            propertyGrid1.SelectedObject = l.Tag;

            selected = (Emp)l.Tag;
            //throw new NotImplementedException();
        }

        private void delete_btn_Click(object sender, EventArgs e)
        {
            //3. den selectierten löschen
            sdc.Emps.DeleteOnSubmit(selected);         
            sdc.SubmitChanges();    //SQL-Delete Befehl ausgeführt
        }

        private void emp_add_btn_Click(object sender, EventArgs e)
        {
            new Einfuegen().ShowDialog();
        }

        private void savechangebtn_Click(object sender, EventArgs e)
        {
            sdc.SubmitChanges();
        }

        private void emp_update_btn_Click(object sender, EventArgs e)
        {
            Form ef = new Einfuegen();
            //ausgewählten MA übergeben
            ef.Tag = selected;
            ef.ShowDialog();
            //und wieder speichern
            sdc.SubmitChanges();
        }
    }
}
