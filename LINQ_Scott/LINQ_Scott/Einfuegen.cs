﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using SewContext;

namespace LINQ_Scott
{
    public partial class Einfuegen : Form
    {
        public Einfuegen()
        {
            InitializeComponent();
        }

        SewContext.SewDataContext sdc = new SewDataContext();

        private void Einfuegen_Load(object sender, EventArgs e)
        {
            //Combobox für Departments-Auswahl füllen
            foreach (var item in sdc.Depts) //geht auch direkt
            {
                deptcombobox.Items.Add(item.DEPTNO);
            }

            //Combobox fürMGR-Auswahl füllen
            foreach (var item in sdc.Emps) //geht auch direkt 
            {
                mgrcombobox.Items.Add(item.EMPNO);
            }            

            //Editier-Mosud=>Vorausfüllen der GUI Elmemente
            if (Tag != null)
            {
                this.Text = "Ändern";

                Emp sel = (Emp)this.Tag;
                enametxtbox.Text = sel.ENAME;
                jobtxtbox.Text = sel.JOB;
                deptcombobox.SelectedItem = sel.DEPTNO;
                mgrcombobox.SelectedItem = sel.MGR;
                //saltxtbox.Text = Convert.ToInt32(sel.SAL);
            }
        }

        private void deptcombobox_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void savebtn_Click(object sender, EventArgs e)
        {
            if (Tag == null)
            {
                Emp ee = new Emp();
                ee.EMPNO = (from d in sdc.Emps select d.EMPNO).Max() + 1;
                ee.ENAME = enametxtbox.Text;
                ee.JOB = jobtxtbox.Text;
                ee.MGR = (int?)Convert.ToInt32(mgrcombobox.Text); //nullable int
                ee.DEPTNO = Convert.ToInt32(deptcombobox.Text);
                ee.HIREDATE = DateTime.Now;
                ee.SAL = Convert.ToInt32(saltxtbox.Text);
                

                sdc.Emps.InsertOnSubmit(ee);
                sdc.SubmitChanges();
            }
            else //updaten
            {
                var curr = (Emp)this.Tag;
                
                curr.ENAME = enametxtbox.Text;
                curr.JOB = jobtxtbox.Text;
                curr.HIREDATE = DateTime.Now;
                //curr.SAL = Convert.ToInt32(saltxtbox.Text);
                curr.MGR = (int?)Convert.ToInt32(mgrcombobox.Text);
                curr.DEPTNO = Convert.ToInt32(deptcombobox.Text);
            }
            sdc.SubmitChanges();

            this.Close();
        }
    }
}
