﻿namespace LINQ_Scott
{
    partial class Einfuegen
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.enametxtbox = new System.Windows.Forms.TextBox();
            this.deptcombobox = new System.Windows.Forms.ComboBox();
            this.savebtn = new System.Windows.Forms.Button();
            this.jobtxtbox = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.saltxtbox = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.mgrcombobox = new System.Windows.Forms.ComboBox();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(13, 13);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(39, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "ename";
            // 
            // enametxtbox
            // 
            this.enametxtbox.Location = new System.Drawing.Point(59, 13);
            this.enametxtbox.Name = "enametxtbox";
            this.enametxtbox.Size = new System.Drawing.Size(100, 20);
            this.enametxtbox.TabIndex = 1;
            // 
            // deptcombobox
            // 
            this.deptcombobox.FormattingEnabled = true;
            this.deptcombobox.Location = new System.Drawing.Point(59, 40);
            this.deptcombobox.Name = "deptcombobox";
            this.deptcombobox.Size = new System.Drawing.Size(100, 21);
            this.deptcombobox.TabIndex = 2;
            this.deptcombobox.SelectedIndexChanged += new System.EventHandler(this.deptcombobox_SelectedIndexChanged);
            // 
            // savebtn
            // 
            this.savebtn.Location = new System.Drawing.Point(197, 226);
            this.savebtn.Name = "savebtn";
            this.savebtn.Size = new System.Drawing.Size(75, 23);
            this.savebtn.TabIndex = 3;
            this.savebtn.Text = "Save";
            this.savebtn.UseVisualStyleBackColor = true;
            this.savebtn.Click += new System.EventHandler(this.savebtn_Click);
            // 
            // jobtxtbox
            // 
            this.jobtxtbox.Location = new System.Drawing.Point(59, 67);
            this.jobtxtbox.Name = "jobtxtbox";
            this.jobtxtbox.Size = new System.Drawing.Size(100, 20);
            this.jobtxtbox.TabIndex = 4;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(14, 74);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(21, 13);
            this.label2.TabIndex = 5;
            this.label2.Text = "job";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(12, 48);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(40, 13);
            this.label3.TabIndex = 6;
            this.label3.Text = "deptno";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(14, 100);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(20, 13);
            this.label4.TabIndex = 8;
            this.label4.Text = "sal";
            // 
            // saltxtbox
            // 
            this.saltxtbox.Location = new System.Drawing.Point(59, 93);
            this.saltxtbox.Name = "saltxtbox";
            this.saltxtbox.Size = new System.Drawing.Size(100, 20);
            this.saltxtbox.TabIndex = 7;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(12, 127);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(25, 13);
            this.label5.TabIndex = 10;
            this.label5.Text = "Mgr";
            // 
            // mgrcombobox
            // 
            this.mgrcombobox.FormattingEnabled = true;
            this.mgrcombobox.Location = new System.Drawing.Point(59, 119);
            this.mgrcombobox.Name = "mgrcombobox";
            this.mgrcombobox.Size = new System.Drawing.Size(100, 21);
            this.mgrcombobox.TabIndex = 9;
            // 
            // Einfuegen
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(284, 261);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.mgrcombobox);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.saltxtbox);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.jobtxtbox);
            this.Controls.Add(this.savebtn);
            this.Controls.Add(this.deptcombobox);
            this.Controls.Add(this.enametxtbox);
            this.Controls.Add(this.label1);
            this.Name = "Einfuegen";
            this.Text = "Einfuegen";
            this.Load += new System.EventHandler(this.Einfuegen_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox enametxtbox;
        private System.Windows.Forms.ComboBox deptcombobox;
        private System.Windows.Forms.Button savebtn;
        private System.Windows.Forms.TextBox jobtxtbox;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox saltxtbox;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.ComboBox mgrcombobox;
    }
}