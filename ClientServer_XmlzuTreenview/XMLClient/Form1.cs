﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Net.Sockets;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Xml;

namespace XMLClient
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {

        }

        private void xml_Click(object sender, EventArgs e)
        {
            TcpClient client = new TcpClient("localhost", 2055);
            try
            {
                Stream s = client.GetStream();
                s.ReadTimeout = 100;
                StreamReader sr = new StreamReader(s);

                StreamWriter sw = new StreamWriter(s);
                sw.AutoFlush = true;

                //Console.WriteLine("schicke Hallo");
                //sw.WriteLine("Hallo");
                //Console.WriteLine("gesendet!");

                string antwort = sr.ReadLine();
                MessageBox.Show(antwort);

                //Console.ReadKey();
                s.Close();
            }
            finally
            {
                // code in finally block is guranteed 
                // to execute irrespective of 
                // whether any exception occurs or does 
                // not occur in the try block
                client.Close();
            }
        }

        XmlDocument xd = new XmlDocument();
        private void treviewbtn_Click(object sender, EventArgs e)
        {
            TcpClient client = new TcpClient("localhost", 2055);
            try
            {
                Stream s = client.GetStream();
                s.ReadTimeout = 100;
                StreamReader sr = new StreamReader(s);

                StreamWriter sw = new StreamWriter(s);
                sw.AutoFlush = true;

                //Console.WriteLine("schicke Hallo");
                //sw.WriteLine("Hallo");
                Console.WriteLine("gesendet!");

                string antwort = sr.ReadLine();

                
                xd.LoadXml(antwort);
                //XmlNode wurzl = xd.SelectSingleNode("DIR");

                //TreeNode root = new TreeNode("DIR");
                //treeView1.Nodes.Add(root);
                XmlNode root = xd.SelectSingleNode("/DIR");
                //TreeNode rootNode = new TreeNode(root.Name); //root.Name
                TreeNode rootNode = new TreeNode(root.Attributes[0].InnerText);
                treeView1.Nodes.Add(rootNode);
                MakeTree(root, rootNode);

                //Console.ReadKey();
                s.Close();
            }
            finally
            {
                // code in finally block is guranteed 
                // to execute irrespective of 
                // whether any exception occurs or does 
                // not occur in the try block
                client.Close();
            }
        }

        private void MakeTree(XmlNode curr, TreeNode current)
        {
            //Für alle Unterverzeichnisse des aktuellen Knotens
            foreach (var item in curr.ChildNodes)
            {
                if (item.Name == "DIR")
                {
                    //Erzeuge neuen TreeNode
                    TreeNode added = current.Nodes.Add(item.Attributes[0].InnerText);
                    //added.Tag = item.FullName;

                    //und rufe Programm rekursiv auf
                    MakeTree((item as XmlNode), added);
                }
            }
        }

        private void treeView1_AfterSelect(object sender, TreeViewEventArgs e)
        {
            try
            {
                string pfad = treeView1.SelectedNode.FullPath;
                //ersetze \ durch / und hänge / vorne dazu damit singlenode das versteht
                pfad = "/" + pfad;
                pfad = pfad.Replace('\\', '/');

                //zerschneiden - mache aus /htl/bau => <DIR[@name='htl']/DIR[@name='bau']
                string[] schnipsel = pfad.Split('/');

                string neu = "";
                foreach (var item in schnipsel)
                {
                    if (item != "")
                        neu += "/DIR[@name=\"" + item + "\"]";
                }

                XmlNode ausgewählt = xd.SelectSingleNode(neu); //pfad
                MessageBox.Show("ausgewählt: " + ausgewählt.Attributes[0].InnerText); //ausgewählt.Name
                string erg = "";
                foreach (XmlNode item in ausgewählt)
                {
                    if (item.Name == "FILE")
                        erg += item./*Name*/Attributes[0].InnerText + "-";
                }
                MessageBox.Show(erg);
            }
            catch (Exception ee) { }
        }

        
    }
}
