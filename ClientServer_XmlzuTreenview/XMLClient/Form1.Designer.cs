﻿namespace XMLClient
{
    partial class Form1
    {
        /// <summary>
        /// Erforderliche Designervariable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Verwendete Ressourcen bereinigen.
        /// </summary>
        /// <param name="disposing">True, wenn verwaltete Ressourcen gelöscht werden sollen; andernfalls False.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Vom Windows Form-Designer generierter Code

        /// <summary>
        /// Erforderliche Methode für die Designerunterstützung.
        /// Der Inhalt der Methode darf nicht mit dem Code-Editor geändert werden.
        /// </summary>
        private void InitializeComponent()
        {
            this.xml = new System.Windows.Forms.Button();
            this.treviewbtn = new System.Windows.Forms.Button();
            this.treeView1 = new System.Windows.Forms.TreeView();
            this.listView1 = new System.Windows.Forms.ListView();
            this.Filename = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.SuspendLayout();
            // 
            // xml
            // 
            this.xml.Location = new System.Drawing.Point(12, 38);
            this.xml.Name = "xml";
            this.xml.Size = new System.Drawing.Size(92, 51);
            this.xml.TabIndex = 0;
            this.xml.Text = "zeige XML";
            this.xml.UseVisualStyleBackColor = true;
            this.xml.Click += new System.EventHandler(this.xml_Click);
            // 
            // treviewbtn
            // 
            this.treviewbtn.Location = new System.Drawing.Point(12, 153);
            this.treviewbtn.Name = "treviewbtn";
            this.treviewbtn.Size = new System.Drawing.Size(92, 51);
            this.treviewbtn.TabIndex = 1;
            this.treviewbtn.Text = "erzeuge Treeview";
            this.treviewbtn.UseVisualStyleBackColor = true;
            this.treviewbtn.Click += new System.EventHandler(this.treviewbtn_Click);
            // 
            // treeView1
            // 
            this.treeView1.Location = new System.Drawing.Point(119, 38);
            this.treeView1.Name = "treeView1";
            this.treeView1.Size = new System.Drawing.Size(212, 166);
            this.treeView1.TabIndex = 2;
            this.treeView1.AfterSelect += new System.Windows.Forms.TreeViewEventHandler(this.treeView1_AfterSelect);
            // 
            // listView1
            // 
            this.listView1.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.Filename});
            this.listView1.Location = new System.Drawing.Point(338, 38);
            this.listView1.Name = "listView1";
            this.listView1.Size = new System.Drawing.Size(434, 166);
            this.listView1.TabIndex = 3;
            this.listView1.UseCompatibleStateImageBehavior = false;
            this.listView1.View = System.Windows.Forms.View.Details;
            // 
            // Filename
            // 
            this.Filename.Text = "Filename";
            this.Filename.Width = 68;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(784, 265);
            this.Controls.Add(this.listView1);
            this.Controls.Add(this.treeView1);
            this.Controls.Add(this.treviewbtn);
            this.Controls.Add(this.xml);
            this.Name = "Form1";
            this.Text = "Form1";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button xml;
        private System.Windows.Forms.Button treviewbtn;
        private System.Windows.Forms.TreeView treeView1;
        private System.Windows.Forms.ListView listView1;
        private System.Windows.Forms.ColumnHeader Filename;
    }
}

